# Contributor: Patrick Jackson <PatrickSJackson gmail com>
# Maintainer: Christoph Vigano <mail@cvigano.de>

pkgname=st
pkgver=0.7
pkgrel=1
pkgdesc='A simple virtual terminal emulator for X.'
arch=('i686' 'x86_64')
license=('MIT')
depends=('libxft' 'libxext' 'xorg-fonts-misc')
makedepends=('ncurses')
url="http://st.suckless.org"
source=(http://dl.suckless.org/st/$pkgname-$pkgver.tar.gz
        config.h
        http://st.suckless.org/patches/st-scrollback-$pkgver.diff
        http://st.suckless.org/patches/st-scrollback-mouse-20161020-6e79e83.diff
        http://st.suckless.org/patches/st-scrollback-mouse-altscreen-20161020-6e79e83.diff)
md5sums=('29b2a599cf1511c8062ed8f025c84c63'
         '27153086854f68c2d62e376a2213ac1d'
         'c55076d94247e121a1a86d32a793cf2e'
         '8bc037ef8ddaff6b9e25839774bf736a'
         '15a5d2e05f4939da14976a9459619956')

prepare() {
  cd $srcdir/$pkgname-$pkgver

  patch -p1 -i "${srcdir}/st-scrollback-$pkgver.diff"
  patch -p1 -i "${srcdir}/st-scrollback-mouse-20161020-6e79e83.diff"
  patch -p1 -i "${srcdir}/st-scrollback-mouse-altscreen-20161020-6e79e83.diff"

  # skip terminfo which conflicts with nsurses
  sed -i '/\@tic /d' Makefile
  cp $srcdir/config.h config.h
}

build() {
  cd $srcdir/$pkgname-$pkgver
  make X11INC=/usr/include/X11 X11LIB=/usr/lib/X11
}

package() {
  cd $srcdir/$pkgname-$pkgver
  make PREFIX=/usr DESTDIR="$pkgdir" TERMINFO="$pkgdir/usr/share/terminfo" install
  install -Dm644 LICENSE "$pkgdir/usr/share/licenses/$pkgname/LICENSE"
  install -Dm644 README "$pkgdir/usr/share/doc/$pkgname/README"
}
